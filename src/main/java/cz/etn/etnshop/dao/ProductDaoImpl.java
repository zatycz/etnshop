package cz.etn.etnshop.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;


@Repository("productDao")
public class ProductDaoImpl extends AbstractDao implements ProductDao {
	
	@SuppressWarnings("unchecked")
	public List<Product> getProducts() {
		Criteria criteria = getSession().createCriteria(Product.class);
		return (List<Product>) criteria.list();
	}
	
	public void addProduct(Product product) {
		Session session = getSession();			
		session.save(product);
	}
	
	public void updateProduct(Product product) {
		Session session = getSession();			
		session.update(product);		
	}
		
	public Product getProductById(int id) {
		Criteria criteria = getSession().createCriteria(Product.class);
		criteria.add(Restrictions.eq("id", id));
		Product product = (Product) criteria.uniqueResult();
		return product;
	}
	
	@SuppressWarnings("unchecked")
	public void deleteProduct(int id) {
		Criteria criteria = getSession().createCriteria(Product.class);
		criteria.add(Restrictions.eq("id", id));
		Product product = (Product) criteria.uniqueResult();
		getSession().delete(product);
	}
	
}
