<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="../header.jsp">
    <jsp:param name="title" value="Edit Product"/>
</jsp:include>

<body>
<div class="container">
    <h2>Product</h2>
    <form:form method="GET" action="/etnshop/product/edit/${product.id}" modelAttribute="product">
        <table class="table">
            <tr>
                <td><form:label path="id">ID</form:label></td>
                <td><form:input path="id" disabled="true"/></td>
            </tr>
            <tr>
                <td><form:label path="name">Name</form:label></td>
                <td><form:input path="name" disabled="true"/></td>
            </tr>
            <tr>
                <td><form:label path="serialNumber">Serial Number</form:label></td>
                <td><form:input path="serialNumber" disabled="true"/></td>
            </tr>
            <tr>
                <td>
                    <a class="btn btn-primary btn-lg" href="/etnshop/product/list" role="button">Back to Products</a>
                </td>
                <td>
                    <a class="btn btn-primary btn-lg" href="/etnshop/product/edit/${product.id}">Edit Product</a>
                    <a class="btn btn-primary btn-lg" href="/etnshop/product/remove/${product.id}">Delete Product</a>
                </td>
            </tr>
        </table>
    </form:form>
</div>
</body>
</html>