<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="../header.jsp">
    <jsp:param name="title" value="Product list"/>
</jsp:include>
<body>
<div class="container">
    <h2>Products</h2>
    <em>Click product ID, Name or Serial number to show product detail.</em>
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Serial Number</th>
            <th>Delete</th>
            <th>Change</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${products}" var="product">
            <tr>
                <td><a href="/etnshop/product/show/${product.id}">${product.id}</a></td>
                <td><a href="/etnshop/product/show/${product.id}">${product.name}</a></td>
                <td><a href="/etnshop/product/show/${product.id}">${product.serialNumber}</a></td>
                <td><a class="btn btn-primary" href="/etnshop/product/remove/${product.id}">Delete</a></td>
                <td><a class="btn btn-primary" href="/etnshop/product/edit/${product.id}">Change</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <hr>
    <p>
        <a class="btn btn-primary btn-lg" href="/etnshop" role="button">Back to homepage</a>
        <a class="btn btn-primary btn-lg" href="/etnshop/product/add" role="button">Add new product</a>
    </p>
    <footer>
        <p>&copy; Etnetera a.s. 2015</p>
    </footer>
</div>

<spring:url value="/resources/core/css/bootstrap.min.js" var="bootstrapJs"/>

<script src="${bootstrapJs}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
</body>
</html>