<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="../header.jsp">
    <jsp:param name="title" value="New Product"/>
</jsp:include>

<body>
<div class="container">
    <h2>New Product</h2>
    <form:form method="POST" action="/etnshop/product/add" modelAttribute="product">
        <table class="table">
            <tr>
                <td><form:label path="name">Name</form:label></td>
                <td><form:input path="name"/></td>
            </tr>
            <tr>
                <td><form:label path="serialNumber">Serial Number</form:label></td>
                <td><form:input path="serialNumber"/></td>
            </tr>
            <tr>
                <td>
                    <a class="btn btn-primary btn-lg" href="/etnshop/product/list" role="button">Back to Products</a>
                </td>
                <td>
                    <input class="btn btn-primary btn-lg" type="submit" value="Save Product"/>
                </td>
            </tr>
        </table>
    </form:form>
</div>
</body>
</html>